﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using SimpleCrudApp.Models;
using SimpleCrudApp.Models.Data;
using System.EnterpriseServices.Internal;
using System.Data.Entity.Migrations;

namespace SimpleCrudApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly DatabaseContext db = new DatabaseContext();

        // GET: Home
        public ActionResult Index()
        {
            List<Person> people = db.People.ToList();

            return View(people);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Person person)
        {
            db.People.Add(person);

            db.SaveChanges();

            List<Person> people = db.People.ToList();

            return RedirectToAction("Index", people);
        }

        public ActionResult Update(int id)
        {

            Person per = db.People.FirstOrDefault(p => p.Id == id);

            return View(per);
        }

        [HttpPost]
        public ActionResult Update(Person person)
        {
            db.People.AddOrUpdate(person);

            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            Person person = db.People.FirstOrDefault(p => p.Id == id);

            if (person != null)
            {
                db.People.Remove(person);
                db.SaveChanges();
                TempData["DeleteMessage"] = "Person deleted successfully";
            }
            else
            {
                TempData["DeleteMessage"] = "Person requested to be deleted not found";
            }

            return RedirectToAction("Index");
        }
    }
}