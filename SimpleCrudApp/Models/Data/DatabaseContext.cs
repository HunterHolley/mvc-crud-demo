﻿using System.Data.Entity;

namespace SimpleCrudApp.Models.Data
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Person> People { get; set; }
    }
}